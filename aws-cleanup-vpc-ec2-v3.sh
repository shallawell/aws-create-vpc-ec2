#!/bin/bash
# @shallawell
# Program Name: aws-create-vpc-ec2.sh
# Purpose: Create full VPC and deploy EC2 instance... plus cleanup
# version 2.0

# first get all the resource vars
FILE=vpc-vars.log
source $FILE

#### Clean up order is important
#terminate Instance_ID
aws ec2 terminate-instances --instance-ids $PUB_INSTANCE_ID $PRIV_INSTANCE_ID
echo "terminating $PUB_INSTANCE_ID $PRIV_INSTANCE_ID..."
aws ec2 wait instance-terminated --instance-ids $PUB_INSTANCE_ID $PRIV_INSTANCE_ID
echo "terminated $PUB_INSTANCE_ID $PRIV_INSTANCE_ID ..."

###
# TODO delete NACLs
# disassoc NACL first
###

# clean up Security Groups
#first clean up dependancies between the groups.
aws ec2 revoke-security-group-ingress --group-id $PRIV_SG_ID --protocol tcp --port 22 --source-group $WEB_SG_ID
#then the SG
aws ec2 delete-security-group --group-id $WEB_SG_ID
aws ec2 delete-security-group --group-id $PRIV_SG_ID

#clean up VPC
echo "terminating Networking ..."
for eachRTB_ASSOC_ID in `aws ec2 describe-route-tables \
--filters "Name=vpc-id,Values=$VPC_ID" "Name=association.subnet-id,Values=$PRIV_SUBNET_ID" "Name=association.main,Values=false" \
--query 'RouteTables[].Associations[].RouteTableAssociationId'`
do
# disassociate routes back to main RTB
aws ec2 disassociate-route-table --association-id $eachRTB_ASSOC_ID
echo "Subnet $PRIV_SUBNET_ID DisAssociation from $RTB_ID completed."
done
for eachRTB_ASSOC_ID in `aws ec2 describe-route-tables \
--filters "Name=vpc-id,Values=$VPC_ID" "Name=association.subnet-id,Values=$PUB_SUBNET_ID" "Name=association.main,Values=false" \
--query 'RouteTables[].Associations[].RouteTableAssociationId'`
do
# disassociate routes back to main RTB
aws ec2 disassociate-route-table --association-id $eachRTB_ASSOC_ID
echo "Subnet $PUB_SUBNET_ID DisAssociation from $RTB_ID completed."
done

aws ec2 delete-route-table --route-table-id $RTB_ID
aws ec2 detach-internet-gateway --internet-gateway-id $IGW_ID --vpc-id $VPC_ID
aws ec2 delete-internet-gateway --internet-gateway-id $IGW_ID
aws ec2 delete-subnet --subnet-id $PUB_SUBNET_ID 
aws ec2 delete-subnet --subnet-id $PRIV_SUBNET_ID
# once subnets are deleted, NACL dependency is removed
aws ec2 delete-network-acl --network-acl-id $NACL_ID
# now delete the VPC
aws ec2 delete-vpc --vpc-id $VPC_ID
echo "terminated $VPC_ID Networking ..."

### Delete tags
for i in ` aws ec2 describe-tags --query "Tags[].ResourceId"`
do
aws ec2 delete-tags --resources $i
done

#rm $FILE
#echo "$FILE deleted."
echo "done."
