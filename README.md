aws-create-vpc-ec2
==================

Author 
------------ 
@shallawell

Description
-----------

aws-create-vpc-ec2-v4.sh
------------------------
A script to CREATE a full VPC with security groups, subnets, IGWs, Routes and EC2 hosts (Pub and Priv) with webserver running on public host

aws-cleanup-vpc-ec2-v3.sh
-------------------------
A script to DELETE a full VPC with security groups, subnets, IGWs, Routes and EC2 hosts (Pub and Priv) with webserver running on public host.
Uses var file from the CREATE script to delete resources.

aws-iam-access-keys.sh
----------------------
A script to manage AWS access keys. An easy way to rotate you access keys.

aws-add-vpc-ncls.sh
-------------------
Adds NACLs to the VPC created. -- ISSUE: THIS IS WIP.

web_instance_bootscript.sh
--------------------------
bash script to bootstrap the EC2 instance with a webserver and some basic content.

aws-rds-basics.sh
-----------------
bash script to Create, List and Delete a RDS db

aws-snapshot-basics.sh
----------------------
bash script to Create EC2 snapshots

aws-list-all-regions-access-keys.sh
-----------------------------------
global access key lookup per region

boto-list-all-regions-access-keys.py
------------------------------------
Python/boto3 global access key lookup per region

boto-rds-basics.py
------------------
Python/boto3 to Create, List and Delete a RDS db

x.* files
---------
These are just archive files for reference.  Use the latest versions for best results.

TODO 
---- 
* Fix add-NACLs script.

License 
-------
Apache License, Version 2.0, January 2004, http://www.apache.org/licenses/ 

