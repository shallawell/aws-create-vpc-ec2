#!/bin/bash
# @shallawell
# Program Name: aws-iam-access-keys.sh
# Purpose: Manage AWS access keys
# version 0.1

# new key content will be created in this  file.

FILE=new_aws_key.txt
#remove the old file fisrt
rm $FILE
### create a key
echo -n "Do you want to create a new Access/Secret key.  (y/n) [ENTER]: "
#get user input
read response2
if [ "$response2" == "y" ]; then
echo "Ok.. Creating a new keys !!!"
aws iam create-access-key --output json | grep Access | tail -2 | tee -a $FILE
#Alternative create key command
#KEY=myIndiaAWSKeytest
#REGION=ap-south-1
#aws ec2 create-key-pair --key-name=$KEY  --region $REGION --query="KeyMaterial" --output=text > ~/.ssh/$KEY.pem
#readonly the key
#chmod 400 ~/.ssh/$KEY.pem
echo "key created."
echo "REMEMBER: You should rotate keys at least once a year!"
echo "$FILE created for Access and Secret Keys"
echo "HINT: Run aws configure to update keys. (you just rotated your keys!)"
else [ "$response2" == "n" ]
echo "Not creating keys."
exit 0
fi


### list a key, save to DELKEY var
#this command LISTS the access keys for current user, sorts by CreateDate, 
#gets the latest AccessKeyId result. awk grabs the Access key (excludes date field)
DELKEY=$(aws iam list-access-keys \
--query 'AccessKeyMetadata[].[AccessKeyId,CreateDate]' \
| sort -r -k 2 | tail -1 | awk {'print $1'})

echo "list-Access-key sorted to find OLDEST key."
echo -n "Key Found : $DELKEY. Do you want to delete this key.  (y/n) [ENTER]: "
#get user input
read response
if [ "$response" == "y" ]; then
echo "you said yes. Deleteing Key in 3 secs!!!"
sleep 3
echo "delete-access-key disabled, NO REAL DELETE OCCURRED"
### delete a key, uncomment to activate the delete function.
#aws iam delete-access-key --access-key-id $DELKEY
echo "deleted $DELKEY"
else [ "$response" == "n" ]
echo "you said no. Not Deleting"
fi

echo "done."

#alternative delete (filter keys by name)
#DEL_KEYS=`aws ec2 describe-key-pairs --output=text --filter "Name=key-name,Values=test*" --query="KeyPairs[].KeyName"`
#for i in $DEL_KEYS; do aws ec2 --dry-run delete-key-pair --key-name $i; done
#
