#!/bin/bash
## UserData Bash script to run-instance
## Option 1
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
# get instance id
curl http://169.254.169.254/latest/meta-data/instance-id > /tmp/id.tmp
ID=`cat /tmp/id.tmp`
echo "</html> <h1> yo! - webserver online, running on instance $ID</h1> </html>" > /var/www/html/index.html

#### Option 2
#-- complete -C '/usr/local/bin/aws_completer' aws
#-- echo "complete -C '/usr/local/aws/bin/aws_completer' aws" >> /home/ec2-user/.bash_profile
#-- echo "export PATH=/usr/local/aws/bin:$PATH"  >> /home/ec2-user/.bash_profile
#
