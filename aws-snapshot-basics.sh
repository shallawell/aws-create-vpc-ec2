#!/bin/bash
# @shallawell
# Program Name: aws-snapshot-basics.sh
# Purpose: interact with AWS EC2 snapshots
# version 1.0

# Find EC2 instances in STOPPED state, get VolumeId
VOLS=`aws ec2 describe-instances --region ap-southeast-2 \
--filter 'Name=instance-state-name,Values=stopped' \
--query 'Reservations[*].Instances[*].[InstanceId,State.Name,BlockDeviceMappings[*].Ebs.VolumeId]' | grep vol`

# loop to create a snapshot of each VolumeId found 
for i in $VOLS
do 
echo "creating snapshot of $i"
aws ec2 create-snapshot --volume-id $i --description "Final Snapshot of $i"
#aws ec2 --dry-run create-snapshot --volume-id $i --description "Final Snapshot of $i"
done

#aws ec2 delete-snapshot --snapshot-id 

echo "done."
