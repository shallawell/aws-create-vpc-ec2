#!/bin/bash
# @shallawell
# Program Name: aws-sns-basics.sh
# Purpose: interact with AWS SNS
# version 1.0

NEW_TOPIC_NAME=stack-updates-topic3
# create a new topic
NEW_TOPIC_ARN=`aws sns create-topic --name $NEW_TOPIC_NAME`
echo "New SNS Topic $NEW_TOPIC_NAME created."

SUB=shallawell@gmail.com
# email subscribe to topic
aws sns subscribe --topic-arn $NEW_TOPIC_ARN \
  --protocol email \
  --notification-endpoint $SUB
echo "subscription email sent, go to your email and confirm the subsciption"
echo "sleeping 30"
# sleep for 30 to allow subscribe, before first test message sent.
sleep 30
echo "sleeping 30, Go confirm the subscription before a test is sent"
# Publish/Test the topic subscription
aws sns publish --topic-arn $NEW_TOPIC_ARN --message "Hello World!"

#list all subscriptions
echo "Listing all SNS Topics"
aws sns list-topics
#list all subscriptions by topic
echo "Listing subscriptions of new topic"
aws sns list-subscriptions-by-topic --topic-arn $NEW_TOPIC_ARN

# unsub from topic
UNSUB_ARN=`aws sns list-subscriptions | grep $NEW_TOPIC_NAME | grep $SUB | awk '{print $5}'`
echo "$SUB will be unsubscribed from $NEW_TOPIC_ARN, using unsubARN of $UNSUB_ARN"
aws sns unsubscribe --subscription-arn $UNSUB_ARN

# delete a topic
DELETE_TOPIC_ARN=$NEW_TOPIC_ARN
#aws sns delete-topic --topic-arn $DELETE_TOPIC_ARN
echo "deleted SNS Topic $NEW_TOPIC_ARN"
echo "done"
