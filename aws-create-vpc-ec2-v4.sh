#!/bin/bash
# @shallawell
# Program Name: aws-create-vpc-ec2.sh
# Purpose: Create full VPC and deploy EC2 instance... plus cleanup
# version 2.0

REGION=ap-south-1
FILE=vpc-vars.log

if [ -e $FILE ]
then
rm $FILE
echo "$FILE deleted to remove old vars."
fi

#create a new keypair if needed.
#KEY=myIndiaAWSKeytest
#aws ec2 create-key-pair --key-name=$KEY  --region $REGION --query="KeyMaterial" --output=text > ~/.ssh/$KEY.pem
#readonly the key
#chmod 400 ~/.ssh/$KEY.pem

# this grabs the first [0] key
SSH_KEY=$(aws ec2 describe-key-pairs --region $REGION --query "KeyPairs[0].KeyName")
echo "REGION=$REGION" >> $FILE
echo "SSH_KEY=$SSH_KEY" >> $FILE

# create a VPC 
CIDR=10.0.0.0/16
aws ec2 create-vpc --cidr-block $CIDR
echo "CIDR=$CIDR" >> $FILE

VPC_ID=$(aws ec2 describe-vpcs \
    --filter "Name=cidr, Values=$CIDR" \
    --query "Vpcs[].VpcId" \
    --output text)

aws ec2 wait vpc-available --vpc-id $VPC_ID
echo "VPC_ID=$VPC_ID" >> $FILE
echo "$VPC_ID created"

# this is needed for public hostnames to be enabled.
aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-hostnames
echo "Enabled DNS Public hostnames"

AZ1=$(aws ec2 describe-availability-zones --query "AvailabilityZones[0].ZoneName")
AZ2=$(aws ec2 describe-availability-zones --query "AvailabilityZones[1].ZoneName")
echo "AZ1=$AZ1" >> $FILE
echo "AZ2=$AZ2" >> $FILE

# create two subnets, one private (10.0.1.0/24) and one public (10.0.2.0/24) 
#define CIDRs
PUB_SUBNET="10.0.1.0/24"
PRIV_SUBNET="10.0.2.0/24"
aws ec2 create-subnet --vpc-id $VPC_ID --availability-zone $AZ1 --cidr-block ${PUB_SUBNET} --output text
aws ec2 create-subnet --vpc-id $VPC_ID --availability-zone $AZ1 --cidr-block ${PRIV_SUBNET} --output text
echo "PUB_SUBNET=$PUB_SUBNET" >> $FILE
echo "PRIV_SUBNET=$PRIV_SUBNET" >> $FILE

PRIV_SUBNET_ID=$(aws ec2 describe-subnets \
    --filter "Name=cidrBlock, Values=${PRIV_SUBNET}" \
    --query "Subnets[].SubnetId" \
    --output text)

PUB_SUBNET_ID=$(aws ec2 describe-subnets \
    --filter "Name=cidrBlock, Values=${PUB_SUBNET}" \
    --query "Subnets[].SubnetId" \
    --output text)

aws ec2 wait subnet-available --subnet-id $PUB_SUBNET_ID
echo "Public Subnet - $PUB_SUBNET_ID created"
aws ec2 wait subnet-available --subnet-id $PRIV_SUBNET_ID
echo "Private Subnet - $PRIV_SUBNET_ID created"
echo "PUB_SUBNET_ID=$PUB_SUBNET_ID" >> $FILE
echo "PRIV_SUBNET_ID=$PRIV_SUBNET_ID" >> $FILE

# modify IP behaviour to allow a Public IP to be assigned on Launch (auto-assign)
aws ec2 modify-subnet-attribute --map-public-ip-on-launch --subnet-id $PUB_SUBNET_ID

# If you want to access ec2 instances over internet, you should attach an internet gateway to your VPC.
M=`aws ec2 create-internet-gateway`
IGW_ID=`echo $M | awk '{print $2}'`
echo "IGW_ID=$IGW_ID" >> $FILE

# Attach the internet gateway to the VPC
aws ec2 attach-internet-gateway --internet-gateway-id $IGW_ID --vpc-id $VPC_ID

# create a route table
aws ec2 create-route-table --vpc-id $VPC_ID

# RouteTables[1] = the newly created RTD_ID as [0] is created by default
RTB_ID=$(aws ec2 describe-route-tables \
    --filters "Name=vpc-id, Values=$VPC_ID" \
    --query "RouteTables[1].RouteTableId" \
    --output text)
echo "RTB_ID=$RTB_ID" >> $FILE

# Attaching internet gateway does not make all the subnets public.
# If you want to make a subnet public, you need to add a route table with internet gateway to subnet.
aws ec2 create-route --route-table-id $RTB_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $IGW_ID

# Associate the route table with BOTH subnets
ASSOC_ID1=`aws ec2 associate-route-table --route-table-id $RTB_ID --subnet-id $PUB_SUBNET_ID`
ASSOC_ID2=`aws ec2 associate-route-table --route-table-id $RTB_ID --subnet-id $PRIV_SUBNET_ID`
echo "ASSOC_ID1=$ASSOC_ID1" >> $FILE
echo "ASSOC_ID2=$ASSOC_ID2" >> $FILE

# create a new security group for Pub Subnet- 1st layer of defence
WEB_SG_ID=$(aws ec2 create-security-group \
    --group-name webDMZ-delete-me-sg \
    --description "This can be deleted" \
    --vpc-id $VPC_ID \
    --output text)
echo "Created Security Group $WEB_SG_ID"

# create a new security group for Priv Subnet- 1st layer of defence
PRIV_SG_ID=$(aws ec2 create-security-group \
    --group-name priv-subnet-delete-me-sg \
    --description "This can be deleted" \
    --vpc-id $VPC_ID \
    --output text)
echo "Created Security Group $PRIV_SG_ID"
echo "WEB_SG_ID=$WEB_SG_ID" >> $FILE
echo "PRIV_SG_ID=$PRIV_SG_ID" >> $FILE


# todo - this could be cleaner - a for loop :)
# authorize-security-group-ingress ALLOW only 1 rule at a time to be defined.
### PRIVATE SUBNET RULES
# allow from Pub to Priv based on Security Group (not cidr)
aws ec2 authorize-security-group-ingress --group-id $PRIV_SG_ID --protocol tcp --port 22 --source-group $WEB_SG_ID
### PUBLIC SUBNET RULES
# allow SSH access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 22 --cidr 0.0.0.0/0
# allow HTTP access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 80 --cidr 0.0.0.0/0
# allow HTTPS access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 443 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 3389 --cidr 0.0.0.0/0

#####
### TODO add NAT Gateway for PRIV_SUBNET_HOSTS to access internal
####

# list top Amazon Linux AMI image for specific region 
AMI_ID=$(aws ec2 describe-images \
 --region $REGION \
 --owners amazon \
 --filters Name=root-device-type,Values=ebs \
  Name=architecture,Values=x86_64 \
  Name=virtualization-type,Values=hvm \
  Name=description,Values="Amazon Linux AMI 2017* GP2" \
 --query "Images[0].ImageId" \
 --output text)

## Create Instances
PUB_INSTANCE_ID=$(aws ec2 run-instances \
    --image-id $AMI_ID \
    --key-name $SSH_KEY \
    --instance-type t2.micro \
    --security-group-ids $WEB_SG_ID \
    --user-data file://web_instance_bootscript.sh \
    --subnet-id $PUB_SUBNET_ID \
    --query "Instances[0].InstanceId" \
    --output text)

PRIV_INSTANCE_ID=$(aws ec2 run-instances \
    --image-id $AMI_ID \
    --key-name $SSH_KEY \
    --instance-type t2.micro \
    --security-group-ids $PRIV_SG_ID \
    --subnet-id $PRIV_SUBNET_ID \
    --query "Instances[0].InstanceId" \
    --output text)
echo "AMI_ID=$AMI_ID" >> $FILE
echo "PUB_INSTANCE_ID=$PUB_INSTANCE_ID" >> $FILE
echo "PRIV_INSTANCE_ID=$PRIV_INSTANCE_ID" >> $FILE

echo "waiting for $PUB_INSTANCE_ID ..."
aws ec2 wait instance-running --instance-ids $PUB_INSTANCE_ID
echo "waiting for $PRIV_INSTANCE_ID ..."
aws ec2 wait instance-running --instance-ids $PRIV_INSTANCE_ID

# create tags
aws ec2 create-tags --resources $PUB_INSTANCE_ID --tags Key=Name,Value=WebServer-delete-me
aws ec2 create-tags --resources $PRIV_INSTANCE_ID --tags Key=Name,Value=PrivateServer-delete-me

PUBLIC_NAME=$(aws ec2 describe-instances \
    --instance-ids $PUB_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PublicDnsName" \
    --output text)

PUB_IP=$(aws ec2 describe-instances \
    --instance-ids $PUB_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PublicIpAddress" \
    --output text)
echo "$PUB_INSTANCE_ID is accepting SSH connections under $PUBLIC_NAME or $PUB_IP"    

PRIV_IP=$(aws ec2 describe-instances \
    --instance-ids $PRIV_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PrivateIpAddress" \
    --output text)

echo "$PRIV_INSTANCE_ID is accepting SSH connections under $PRIV_IP, but only from the Public instance, not 0.0.0.0/0" | tee -a $FILE
echo "HINT: Use ssh-agent bash; ssh-add  ~/.ssh/$SSH_KEY.pem to add identity to ssh-agent" | tee -a $FILE 
echo "HINT: Use ssh -A ec2-user@$PUBLIC_NAME to connect" | tee -a $FILE 
echo "HINT: From the Public Instance, use ssh ec2-user@$PRIV_IP to connect to private instance" | tee -a $FILE 
echo "HINT: From a browser, use http://$PUBLIC_NAME to see the running web server, created from bootstrap script" | tee -a $FILE 
echo "\n"
echo "Created $FILE to track the new resources (helps with the delete process)."
echo "done."

