#!/bin/bash
# @shallawell Program Name: git-delete-remote-project.sh Version = 0.1.0 Can delete a Gitlab repo & the contents. start 
# ===============================
#
#set vars
KEYNAME=$1
KEYFILE=~/.ssh/$1.pem

#usage
if [ ${#@} == 0 ]; then
    echo "Error : newKeyName name required."
    echo "Usage: $0 my<region>AWSKey"
    exit 0
fi

echo "creating $1 ssh key"
aws ec2 create-key-pair --key-name $1 --query 'KeyMaterial' --output text > $KEYFILE
echo "setting correct permissions - chmod 400 $KEYFILE"
chmod 400 $KEYFILE
