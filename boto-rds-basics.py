#!/usr/bin/python
from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
from pprint import pprint
from collections import OrderedDict

DBNAME='deletemedb'
client = boto3.client('rds')

myCreateDB =  client.create_db_instance(
    DBName=DBNAME,
    DBInstanceIdentifier= ('%s-DELETEME'% DBNAME),
    DBInstanceClass='db.t2.micro',
    AllocatedStorage=5,
    Engine='MySQL',
    EngineVersion='5.7.17',
    MultiAZ=False,
    PubliclyAccessible=False,
    MasterUserPassword='myPassword',
    MasterUsername='dbadmin',
)
#create the db
myCreateDB

myInstance = client.describe_db_instances(
    DBInstanceIdentifier= ('%s--DELETEME'% DBNAME)
)

print(myInstance)

ARN='arn:aws:rds:ap-south-1:xxxxxxxxxxxxx:db:deletemedb'
print('Make sure %s this does not contain xxxxxxxxxxxxxx'% ARN)
myTags = client.list_tags_for_resource(
            ResourceName=('%s'% ARN),
            Filters=[{'Name' : 'Environment', 'Values' : ['Production']}])

print(myTags)
#<snip>
# Fails (as expected per docs)!
# Filter on Name/Values NOT IMPLEMENTED
#botocore.exceptions.ClientError: An error occurred (InvalidParameterValue) when calling the ListTagsForResource operation: Unrecognized filter name: Environment
#</snip>

DBNAME='deletemedb'
mydeleteDB = client.delete_db_instance(
    DBInstanceIdentifier= ('%s-prod'% DBNAME),
    SkipFinalSnapshot=True
    )

mydeleteDB

#check
myInstance = client.describe_db_instances(
    DBInstanceIdentifier= ('%s-prod'% DBNAME)
)

pprint(myInstance)

###TEST
myInstance = client.describe_db_instances(
    DBInstanceIdentifier= ('%s-prod'% DBNAME)
)

###JSON STUFF NEEDS WORK 
## trying to format and print specific fields from the myInstance output
results = json.loads((myInstance).decode('utf-8'))
pprint(results, indent=4, depth=2 )
#dataDict = json.loads(myInstance, object_pairs_hook=JSONObject)
# use pprint to  pretty-print json
json1_data_dict = (myInstance)
name = instance[]
print('Name of instance:', name)

print('done')
