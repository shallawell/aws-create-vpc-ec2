REGIONS=`aws ec2 describe-regions --query="Regions[].RegionName"`
for i in $REGIONS; do echo "AZs for Region $i"; aws ec2 describe-availability-zones --region $i | wc -l; done

