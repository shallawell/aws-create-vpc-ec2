#!/bin/bash
# @shallawell
# Program Name: aws-create-vpc-ec2.sh
# Purpose: Create full VPC and deploy EC2 instance... plus cleanup

SSH_KEY=$(aws ec2 describe-key-pairs --query "KeyPairs[0].KeyName")

# create a VPC 
aws ec2 create-vpc --cidr-block 10.0.0.0/16

VPC_ID=$(aws ec2 describe-vpcs \
    --filter "Name=isDefault, Values=true" \
    --query "Vpcs[0].VpcId" \
    --output text)

aws ec2 wait vpc-available --vpc-id $VPC_ID
echo "$VPC_ID created"
# create two subnets, one private (10.0.1.0/24) and one public (10.0.2.0/24) 
aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.1.0/24
aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.2.0/24

# If you want to access ec2 instances over internet, you should attach an internet gateway to your VPC.
aws ec2 create-internet-gateway

IGW_ID=$(aws ec2 describe-internet-gateways \
    --query "InternetGateways[0].InternetGatewayId" \
    --output text)

# Attach the internet gateway to the VPC
aws ec2 attach-internet-gateway --internet-gateway-id $IGW_ID --vpc-id $VPC_ID

# create a route table
aws ec2 create-route-table --vpc-id $VPC_ID

RTB_ID=$(aws ec2 describe-route-tables \
    --query "RouteTables[0].RouteTableId" \
    --output text)

# Attaching internet gateway does not make all the subnets public.
# If you want to make a subnet public, you need to add a route table with internet gateway to subnet.
aws ec2 create-route --route-table-id $RTB_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $IGW_ID

PUB_SUBNET_ID=$(aws ec2 describe-subnets \
    --filters "Name=vpc-id, Values=$VPC_ID" \
    --query "Subnets[1].SubnetId" \
    --output text)

#  pick the 2nd found subnet
PRIV_SUBNET_ID=$(aws ec2 describe-subnets \
    --filters "Name=vpc-id, Values=$VPC_ID" \
    --query "Subnets[1].SubnetId" \
    --output text)

# Associate the route table with the second subnet (10.0.2.0/24)
aws ec2 associate-route-table --route-table-id rtb-????????? --subnet-id $PRIV_SUBNET_ID

# create a new security group
SG_ID=$(aws ec2 create-security-group \
    --group-name delete-me-sg \
    --description "This can be deleted" \
    --vpc-id $VPC_ID \
    --output text)

# allow SSH access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $SG_ID \
    --protocol tcp --port 22 --cidr 0.0.0.0/0


# list top Amazon Linux AMI image for specific region 
AMI_ID=`aws ec2 describe-images \
 --region ap-southeast-2 \
 --owners amazon \
 --filters Name=root-device-type,Values=ebs \
  Name=architecture,Values=x86_64 \
  Name=virtualization-type,Values=hvm \
  Name=description,Values="Amazon Linux AMI 2017* GP2" \
 --query "Images[0].ImageId" \
 --output text`

# Launch Instance
INSTANCE_ID=$(aws ec2 run-instances \
    --image-id $AMI_ID \
    --key-name $SSH_KEY \
    --instance-type t2.micro \
    --security-group-ids $SG_ID \
    --subnet-id $PRIV_SUBNET_ID \
    --query "Instances[0].InstanceId" \
    --output text)

echo "waiting for $INSTANCE_ID ..."
aws ec2 wait instance-running --instance-ids $INSTANCE_ID

# create tags
aws ec2 create-tags --resources $INSTANCE_ID --tags Key=Name,Value=WebServer

PUBLIC_NAME=$(aws ec2 describe-instances \
    --instance-ids $INSTANCE_ID \
    --query "Reservations[0].Instances[0].PublicDnsName" \
    --output text)

echo "$INSTANCE_ID is accepting SSH connections under $PUBLIC_NAME"    
#terminate Instance_ID
aws ec2 terminate-instances --instance-ids $INSTANCE_ID
echo "terminating $INSTANCE_ID ..."
aws ec2 wait instance-terminated --instance-ids $INSTANCE_ID
# clean up SG
aws ec2 delete-security-group --group-id $SG_ID

#clean up VPC
echo "terminating Networking ..."
aws ec2 delete-route-table-id $RTB_ID
aws ec2 detach-internet-gateway --gateway-id $IGW_ID
aws ec2 delete-internet-gateway --gateway-id $IGW_ID
aws ec2 delete-subnet --subnet-id $PUB_SUBNET_ID --subnet-id $PRIV_SUBNET_ID
aws ec2 delete-vpc --vpc-id $VPC_ID
echo "done."
