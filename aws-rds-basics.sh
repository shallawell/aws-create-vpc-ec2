#!/bin/bash
# @shallawell
# Program Name: aws-rds-basics.sh
# Purpose: interact with AWS RDS
# version 1.0
# get a list of RDS Engines version for mysql
aws rds describe-db-engine-versions --output json --query 'DBEngineVersions[]|[?contains(Engine, `mysql`) == `true`].[Engine,DBEngineVersionDescription]'

# get the latest mysql engine version
aws rds describe-db-engine-versions --query 'DBEngineVersions[]|[?contains(Engine, `mysql`) == `true`].[Engine,DBEngineVersionDescription]' | sort -r -k 2 | head -1

#describe db.instance
aws rds describe-db-instances --output json --query 'DBInstances[].[DBInstanceIdentifier,Engine,EngineVersion,DBInstanceArn]'

# list db.instance tags
aws rds list-tags-for-resource \
--resource-name arn:aws:rds:ap-south-1:965010732742:db:deletemedb \
--output json 
--query 'TagList[]|[?contains(Value, `Production`) == `true`].[Key, Value]'


# create db.instance
aws rds create-db-instance \
--db-name deleteMeDB \
--db-instance-identifier deleteMeDB \
--allocated-storage 5 \
--db-instance-class db.t2.micro \
--engine mysql \
--engine-version 5.7.17 \
--master-username dbadmin \
--master-user-password myPassword \
--tags Key=Environment,Value=Production Key=Name,Value=deleteMeDB \
--no-publicly-accessible \
--no-multi-az

echo "done."
