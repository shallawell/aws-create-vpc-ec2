#!/bin/bash
# @shallawell
# Program Name: aws-list-all-regions-access-keys.sh
# Purpose: List all AWS access globally (per region)
# version 0.1

# list ALL access keys access all REGIONS.
REGIONS=`aws ec2 describe-regions --query="Regions[].RegionName"`
for i in $REGIONS
do echo "Access Keys for Region $i"
aws ec2 describe-key-pairs --region $i
done
echo "done."

