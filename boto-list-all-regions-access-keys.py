#!/usr/bin/python
from __future__ import print_function # Python 2/3 compatibility
import boto3

client = boto3.client('ec2')  
regions = [region['RegionName'] for region in client.describe_regions()['Regions']]

for region in regions:
    print("Finding Keys in region %s"% (region))
    client = boto3.client('ec2', region_name=region)
    keys = client.describe_key_pairs(
    KeyNames=[]
    )
# NEED TO FIGURE OUT HOW TO FILTER THE KEYNAME VALUE ONLY AS A STRING (maybe json.load/dump???)
#    keys['KeyPairs']
#    print "Found Keys  {}".format(keys)
    print("Region: %s "% (region))
#    print("Keys: %s"% (keys['KeyPairs']))
    print("Keys: %s"% (keys['KeyPairs']))

print('done')
