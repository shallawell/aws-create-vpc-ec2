#!/bin/bash
# @shallawell
# Program Name: aws-create-vpc-ec2.sh
# Purpose: Create NACLS for the VPC_ID
# version 1.0

FILE=vpc-vars.log
source $FILE

##
# AWS Documentation on VPC & NACLs
# http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Appendix_NACLs.html#VPC_Appendix_NACLs_Scenario_2
##

### Network-ACLs - 2nd layer of defence
# create a new ACL
PUB_NACL_ID=$(aws ec2 create-network-acl --vpc-id $VPC_ID --query="NetworkAcl.NetworkAclId" --output=text)
echo "NACL $PUB_NACL_ID created in VPC $VPC_ID"
echo "PUB_NACL_ID=$PUB_NACL_ID" >> $FILE

##### create ingress and egress NACLs
#define beginning rule number
RULENBR=100
# declare a string array - ADD NEW NACL PORTS HERE  -- ICMP has be excluded to dent PING from internet (even tho SG allows it)
declare -a ALLOWPORTS=("22" "3389")
# define the Protocol tcp | udp | icmp
ALLOWPROTO=tcp
# loop thru array
for PORT in "${ALLOWPORTS[@]}"
do
echo "Creating ingress and egress Rule $RULENBR --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT"
aws ec2 create-network-acl-entry --network-acl-id $PUB_NACL_ID \
--ingress --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT --cidr-block 0.0.0.0/0 --rule-action allow
aws ec2 create-network-acl-entry --network-acl-id $PUB_NACL_ID \
--egress --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT --cidr-block 0.0.0.0/0 --rule-action allow
#increment next rule number by 100
RULENBR=`expr $RULENBR + 100`
done
# extra rules (not in loop)
# add a ephemeral port range rule SSH for outbound
RULENBR=`expr $RULENBR + 100`
aws ec2 create-network-acl-entry --network-acl-id $PUB_NACL_ID \
--egress --rule-number $RULENBR --protocol tcp --port-range From=1024,To=65535 --cidr-block 0.0.0.0/0 --rule-action allow
echo "Public Subnet NACL created. - VPC Perimeter is more secure."

###
# TODO create a private subnet NACL to allow SSH from PUB_SUB to PRIV_SUB.
###


echo "All NACLs created. - VPC Perimeter is more secure."

#####ISSUE - this needs fixing !!
###
# TODO Redo replace-network-acl-assoc
# 1. for pub sub
# 2. for priv sub
#aws ec2 replace-network-acl-association --association-id XXXXXXX --network-acl-id $PUB_NACL_ID
#aws ec2 replace-network-acl-association --association-id XXXXXXX --network-acl-id $PRIV_NACL_ID
###

# Get current assoc_ID for default NACL.
#for eachASSOC_ID in `aws ec2 describe-network-acls \
#--filters "Name=vpc-id,Values=$VPC_ID" "Name=default,Values=true" \
#--query 'NetworkAcls[].Associations[].NetworkAclAssociationId'`
#do
# Replace subnet association to new $NACL_ID
#aws ec2 replace-network-acl-association --association-id $eachASSOC_ID --network-acl-id $NACL_ID
#echo "Subnet Associated $eachASSOC_ID to $NACL_ID completed."
#done

echo "done."
