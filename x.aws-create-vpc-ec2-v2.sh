#!/bin/bash
# @shallawell
# Program Name: aws-create-vpc-ec2.sh
# Purpose: Create full VPC and deploy EC2 instance... plus cleanup
# version 2.0

REGION=ap-south-1

#create a new keypair if needed.
#KEY=myIndiaAWSKeytest
#aws ec2 create-key-pair --key-name=$KEY  --region $REGION --query="KeyMaterial" --output=text > ~/.ssh/$KEY.pem
#readonly the key
#chmod 400 ~/.ssh/$KEY.pem

# this grabs the first [0] key
SSH_KEY=$(aws ec2 describe-key-pairs --region $REGION --query "KeyPairs[0].KeyName")

# create a VPC 
aws ec2 create-vpc --cidr-block 10.0.0.0/16

VPC_ID=$(aws ec2 describe-vpcs \
    --filter "Name=cidr, Values=10.0.0.0/16" \
    --query "Vpcs[].VpcId" \
    --output text)

aws ec2 wait vpc-available --vpc-id $VPC_ID
echo "$VPC_ID created"

# this is needed for public hostnames to be enabled.
aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-hostnames
echo "Enabled DNS Public hostnames"

AZ1=$(aws ec2 describe-availability-zones --query "AvailabilityZones[0].ZoneName")
AZ2=$(aws ec2 describe-availability-zones --query "AvailabilityZones[1].ZoneName")

# create two subnets, one private (10.0.1.0/24) and one public (10.0.2.0/24) 
#define CIDRs
PUB_SUBNET="10.0.1.0/24"
PRIV_SUBNET="10.0.2.0/24"
aws ec2 create-subnet --vpc-id $VPC_ID --availability-zone $AZ1 --cidr-block ${PUB_SUBNET} --output text
aws ec2 create-subnet --vpc-id $VPC_ID --availability-zone $AZ1 --cidr-block ${PRIV_SUBNET} --output text

PRIV_SUBNET_ID=$(aws ec2 describe-subnets \
    --filter "Name=cidrBlock, Values=${PRIV_SUBNET}" \
    --query "Subnets[].SubnetId" \
    --output text)

PUB_SUBNET_ID=$(aws ec2 describe-subnets \
    --filter "Name=cidrBlock, Values=${PUB_SUBNET}" \
    --query "Subnets[].SubnetId" \
    --output text)

aws ec2 wait subnet-available --subnet-id $PUB_SUBNET_ID
echo "Public Subnet - $PUB_SUBNET_ID created"
aws ec2 wait subnet-available --subnet-id $PRIV_SUBNET_ID
echo "Private Subnet - $PRIV_SUBNET_ID created"

# modify IP behaviour to allow a Public IP to be assigned on Launch (auto-assign)
aws ec2 modify-subnet-attribute --map-public-ip-on-launch --subnet-id $PUB_SUBNET_ID

# If you want to access ec2 instances over internet, you should attach an internet gateway to your VPC.
aws ec2 create-internet-gateway

IGW_ID=$(aws ec2 describe-internet-gateways \
    --query "InternetGateways[0].InternetGatewayId" \
    --output text)

# Attach the internet gateway to the VPC
aws ec2 attach-internet-gateway --internet-gateway-id $IGW_ID --vpc-id $VPC_ID

# create a route table
aws ec2 create-route-table --vpc-id $VPC_ID

# RouteTables[1] = the newly created RTD_ID as [0] is created by default
RTB_ID=$(aws ec2 describe-route-tables \
    --filters "Name=vpc-id, Values=$VPC_ID" \
    --query "RouteTables[1].RouteTableId" \
    --output text)

# Attaching internet gateway does not make all the subnets public.
# If you want to make a subnet public, you need to add a route table with internet gateway to subnet.
aws ec2 create-route --route-table-id $RTB_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $IGW_ID

######## THIS CAN BE REMOVED - REDUNDANT
#PUB_SUBNET_ID=$(aws ec2 describe-subnets \
#    --filters "Name=vpc-id, Values=$VPC_ID" \
#    --query "Subnets[0].SubnetId" \
#    --output text)

#  pick the 2nd found subnet
#PRIV_SUBNET_ID=$(aws ec2 describe-subnets \
#    --filters "Name=vpc-id, Values=$VPC_ID" \
#    --query "Subnets[1].SubnetId" \
#    --output text)
#########

# Associate the route table with BOTH subnets
aws ec2 associate-route-table --route-table-id $RTB_ID --subnet-id $PUB_SUBNET_ID
aws ec2 associate-route-table --route-table-id $RTB_ID --subnet-id $PRIV_SUBNET_ID

# create a new security group for Pub Subnet- 1st layer of defence
WEB_SG_ID=$(aws ec2 create-security-group \
    --group-name webDMZ-delete-me-sg \
    --description "This can be deleted" \
    --vpc-id $VPC_ID \
    --output text)
echo "Created Security Group $WEB_SG_ID"

# create a new security group for Priv Subnet- 1st layer of defence
PRIV_SG_ID=$(aws ec2 create-security-group \
    --group-name priv-subnet-delete-me-sg \
    --description "This can be deleted" \
    --vpc-id $VPC_ID \
    --output text)
echo "Created Security Group $PRIV_SG_ID"

# todo - this could be cleaner - a for loop :)
# authorize-security-group-ingress ALLOW only 1 rule at a time to be defined.
### PRIVATE SUBNET RULES
# allow from Pub to Priv based on Security Group (not cidr)
aws ec2 authorize-security-group-ingress --group-id $PRIV_SG_ID --protocol tcp --port 22 --source-group $WEB_SG_ID
#aws ec2 authorize-security-group-ingress --group-id $PRIV_SG_ID --protocol icmp --port -1 --source-group $WEB_SG_ID
### PUBLIC SUBNET RULES
# allow SSH access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 22 --cidr 0.0.0.0/0
# allow HTTP access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 80 --cidr 0.0.0.0/0
# allow HTTPS access to the security group from ANY
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 443 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol tcp --port 3389 --cidr 0.0.0.0/0
# allow ICMP/PING access to the security group from ANY
#aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol icmp --port -1 --cidr 0.0.0.0/0
# allow ping from Priv to Pub
#aws ec2 authorize-security-group-ingress --group-id $WEB_SG_ID --protocol icmp --port -1 --source-group $PRIV_SG_ID

### Network-ACLs - 2nd layer of defence
# create a new ACL
#NACL_ID=$(aws ec2 create-network-acl --vpc-id $VPC_ID --query="NetworkAcl.NetworkAclId" --output=text)
#echo "NACL $NACL_ID created in VPC $VPC_ID"

##### create ingress and egress NACLs
#define beginning rule number
#RULENBR=100
# declare a string array - ADD NEW NACL PORTS HERE  -- ICMP has be excluded to dent PING from internet (even tho SG allows it)
#declare -a ALLOWPORTS=("80" "443" "22" "3389")
# define the Protocol tcp | udp | icmp
#ALLOWPROTO=tcp
# loop thru array
#for PORT in "${ALLOWPORTS[@]}"
#do
#echo "Creating ingress and egress Rule $RULENBR --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT"
#aws ec2 create-network-acl-entry --network-acl-id $NACL_ID --ingress --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT --cidr-block 0.0.0.0/0 --rule-action allow
#aws ec2 create-network-acl-entry --network-acl-id $NACL_ID --egress --rule-number $RULENBR --protocol $ALLOWPROTO --port-range From=$PORT,To=$PORT --cidr-block 0.0.0.0/0 --rule-action allow
#increment next rule number by 100
#RULENBR=`expr $RULENBR + 100`
#done
#echo "All NACLs created. - VPC Perimeter is more secure."

###
# Get current assoc_ID for default NACL.
#for eachASSOC_ID in `aws ec2 describe-network-acls --filters "Name=vpc-id,Values=$VPC_ID" "Name=default,Values=true" --query 'NetworkAcls[].Associations[].NetworkAclAssociationId'`
#do
# Replace subnet association to new $NACL_ID
#aws ec2 replace-network-acl-association --association-id $eachASSOC_ID --network-acl-id $NACL_ID
#echo "Subnet Associated $eachASSOC_ID to $NACL_ID completed."
#done

#####
### TODO add NAT Gateway for PRIV_SUBNET_HOSTS to access internal
####

# list top Amazon Linux AMI image for specific region 
AMI_ID=$(aws ec2 describe-images \
 --region $REGION \
 --owners amazon \
 --filters Name=root-device-type,Values=ebs \
  Name=architecture,Values=x86_64 \
  Name=virtualization-type,Values=hvm \
  Name=description,Values="Amazon Linux AMI 2017* GP2" \
 --query "Images[0].ImageId" \
 --output text)

## Create Instances
PUB_INSTANCE_ID=$(aws ec2 run-instances \
    --image-id $AMI_ID \
    --key-name $SSH_KEY \
    --instance-type t2.micro \
    --security-group-ids $WEB_SG_ID \
    --subnet-id $PUB_SUBNET_ID \
    --query "Instances[0].InstanceId" \
    --output text)

PRIV_INSTANCE_ID=$(aws ec2 run-instances \
    --image-id $AMI_ID \
    --key-name $SSH_KEY \
    --instance-type t2.micro \
    --security-group-ids $PRIV_SG_ID \
    --subnet-id $PRIV_SUBNET_ID \
    --query "Instances[0].InstanceId" \
    --output text)



## add UserData Bash script to run-instance
#### Option 1
## yum update -y
## yum install httpd
## service httpd start
## chkconfig httpd on
## echo "<!/html> yo! </html>" > /var/www/html/index.html
#### Option 2
#-- complete -C '/usr/local/bin/aws_completer' aws
#-- echo "complete -C '/usr/local/aws/bin/aws_completer' aws" >> /home/ec2-user/.bash_profile
#-- echo "export PATH=/usr/local/aws/bin:$PATH"  >> /home/ec2-user/.bash_profile
####


echo "waiting for $PUB_INSTANCE_ID ..."
aws ec2 wait instance-running --instance-ids $PUB_INSTANCE_ID

echo "waiting for $PRIV_INSTANCE_ID ..."
aws ec2 wait instance-running --instance-ids $PRIV_INSTANCE_ID

# create tags
aws ec2 create-tags --resources $PUB_INSTANCE_ID --tags Key=Name,Value=WebServer-delete-me
# create tags
aws ec2 create-tags --resources $PRIV_INSTANCE_ID --tags Key=Name,Value=PrivateServer-delete-me

PUBLIC_NAME=$(aws ec2 describe-instances \
    --instance-ids $PUB_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PublicDnsName" \
    --output text)

PUB_IP=$(aws ec2 describe-instances \
    --instance-ids $PUB_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PublicIpAddress" \
    --output text)
echo "$PUB_INSTANCE_ID is accepting SSH connections under $PUBLIC_NAME or $PUB_IP"    

PRIV_IP=$(aws ec2 describe-instances \
    --instance-ids $PRIV_INSTANCE_ID \
    --query "Reservations[0].Instances[0].PrivateIpAddress" \
    --output text)

echo "$PRIV_INSTANCE_ID is accepting SSH connections under $PRIV_IP, but only from the Public instance, not 0.0.0.0/0"    
echo "HINT: Use ssh ec2-user@$PUBLIC_NAME -i ~/.ssh/$SSH_KEY.pem to connect"
echo "HINT: Use ssh ec2-user@$PRIV_IP -i ~/.ssh/$SSH_KEY.pem to connect to private instance"

sleep 10000
echo "sleeping for 10000sec.  GET TESTING!!!! control-C to quit."

#### Clean up order is important
#terminate Instance_ID
aws ec2 terminate-instances --instance-ids $PUB_INSTANCE_ID $PRIV_INSTANCE_ID
echo "terminating $PUB_INSTANCE_ID $PRIV_INSTANCE_ID..."
aws ec2 wait instance-terminated --instance-ids $PUB_INSTANCE_ID $PRIV_INSTANCE_ID
echo "terminated $PUB_INSTANCE_ID $PRIV_INSTANCE_ID ..."
# clean up Security Groups
#first clean up dependancies between the groups.
aws ec2 revoke-security-group-ingress --group-id $PRIV_SG_ID --protocol tcp --port 22 --source-group $WEB_SG_ID
aws ec2 revoke-security-group-ingress --group-id $PRIV_SG_ID --protocol icmp --port -1 --source-group $WEB_SG_ID
#then the SG
aws ec2 delete-security-group --group-id $WEB_SG_ID
aws ec2 delete-security-group --group-id $PRIV_SG_ID

#clean up VPC
echo "terminating Networking ..."
aws ec2 delete-route-table --route-table-id $RTB_ID
aws ec2 detach-internet-gateway --internet-gateway-id $IGW_ID --vpc-id $VPC_ID
aws ec2 delete-internet-gateway --internet-gateway-id $IGW_ID
aws ec2 delete-subnet --subnet-id $PUB_SUBNET_ID 
aws ec2 delete-subnet --subnet-id $PRIV_SUBNET_ID
aws ec2 delete-vpc --vpc-id $VPC_ID
echo "done."
